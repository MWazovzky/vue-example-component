import { shallowMount } from '@vue/test-utils';
import ExampleComponent from '@/ExampleComponent';

describe('ExampleComponent.vue', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallowMount(ExampleComponent, { 
            propsData: {
                initialCounter: 123
            }
        })
    })

    it('should render initial state', () => {
        // props
        expect(wrapper.vm.initialCounter).toBe(123)
        // data
        expect(wrapper.vm.counter).toBe(123)
        // computed props
        expect(wrapper.vm.next).toBe(124)
        // rendered template
        expect(wrapper.contains('h1')).toBe(true)
        const h1 = wrapper.find('h1')
        expect(h1.html()).toMatch(/Count/)
        expect(h1.html()).toMatch(/123/)
        expect(h1.html()).toMatch(/[24]/)
        const button = wrapper.find('[jest="increment-button"]')
        expect(button.text()).toBe('+1')
    })

    it('should increase counter', () => {
        // Given
        expect(wrapper.vm.counter).toBe(123)
        // When
        const button = wrapper.find('[jest="increment-button"]')
        button.trigger('click')
        // Then
        expect(wrapper.vm.counter).toBe(124)
    })
})
