# Vue Single File Component template
Share vue SFC as npm package

## Create user-scoped package
```
npm init --scope=@mwazovzky
```

## Serve component
```
vue serve ./src/ExampleComponent.vue
```

## Run build process
```
npm run build
```

## Run tests
```
npm run test
```

## Publish user-scoped package
```
npm publish --access public
```

## Import and use component
```
npm install @mwazovzky/vue-example-component
```

```js
// Test.vue
<template>
    <div>
        <h1>Test</h1>
        <ExampleComponent/>
    </div>
</template>

<script>
import ExampleComponent from '@mwazovzky/vue-example-component'
export default {
    components: {
        ExampleComponent
    }
}
</script>
```

[Instructiions](https://vuejs.org/v2/cookbook/packaging-sfc-for-npm.html)
